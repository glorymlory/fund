import React, {Component} from 'react';
import cx from 'classnames';
// import PropTypes from 'prop-types';
import {Link, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {NavItem, NavLink, TabContent, FormGroup, Input, Button} from 'reactstrap';
// import Widget from '../../components/Widget';
// import {fetchPosts} from '../../actions/posts';
import s from './WelcomePage.scss';
import history from '../../history';
import HeaderLanding from "../../components/HeaderLanding/HeaderLanding";
import Footer from '../../components/Footer/Footer';
// import Widget from '../../components/Widget';

// import { fetchPosts } from '../../actions/posts';
// import s from './Dashboard.scss';
// import Header from "../../components/Header";

class WelcomePage extends Component {
  async componentDidMount() {
    // console.log()
    // this.props.dispatch(poolActions.getAllPools());
  }

  onButtonClicked = (link) => {
    history.push(link)
}

  render() {
    return (
      <div className={s.landing}>
        <HeaderLanding/>
        <main className={s.content}>
        <div
          className={cx("position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-gr-light", s["overflow-hidden"], s["bg-gr-light"])}>
          <div className="col-md-5 p-lg-5 mx-auto my-5">
            <h1 className="display-4 font-weight-normal">Blockchain Funds Sharing</h1>
            <p className="lead font-weight-normal">The Blockchain service for creating funds and sharing pools.</p>
            <a className={cx("btn btn-outline-inverse btn-opt", s["btn-opt"])} href="#">Coming soon</a>
          </div>
        </div>
        <div className={cx("d-md-flex flex-md-equal w-100 my-md-3 pl-md-3", s["flex-md-equal"])}>
          <div
            className={cx("bg-dark mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center text-white overflow-hidden", s["overflow-hidden"])}>
            <div className="my-3 p-3">
              <Button className="width-100 mb-xs mr-xs btn btn-primary btn-lg"
                      onClick={() => this.onButtonClicked("/login")}>
                Create fund</Button>
              <p className="lead">Quickly create a customized pool and start gathering funds for an ICO, a presale, or
                your grandma's  birthday.</p>
            </div>
            <div className={cx('mx-auto specDiv bg-gr-light', s.specDiv, s["bg-gr-light"])} />
          </div>
          <div
            className={cx("mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden bg-gr-light", s["overflow-hidden"], s["bg-gr-light"])}>
            <div className="my-3 py-3">
              <Button className="width-100 mb-xs mr-xs btn btn-primary btn-lg"
                      onClick={() => this.onButtonClicked("/login")}>Become Investor</Button>
              <p className="lead">Track all your pools and contributions. You can check up on a pool's status,
                distribution of tokens, pool ratings...</p>
            </div>
            <div className={cx('bg-dark mx-auto', 'specDiv', s.specDiv)} />
          </div>
        </div>

        {/* <div style={{backgroundColor: "#fff"}}> */}
        {/* <div style={{paddingTop: "118px"}}> */}
        {/* <div className="mainBanner"> */}
        {/* <p className="mainBanerH1" style={{color: "#fff"}}>Family is why.</p> */}
        {/* <p style={{fontSize: "3rem", color: "#fff"}}>Life is why we walk. Whats your reason?</p> */}
        {/* </div> */}
        {/* </div> */}

        {/* <div className={cx('mb-0', s.welcomePage)}> */}
        {/* <div className="mainPageBtnBlock"> */}
        {/* <Link to="/dashboard/create/pool"> */}
        {/* <Button bsStyle="danger" bsSize="large" className="mainPageBtn">Create pool</Button> */}
        {/* </Link> */}
        {/* </div> */}
        {/* <div className="mainPageBtnBlock"> */}
        {/* <Link to="/dashboard/create/fund"> */}
        {/* <Button bsStyle="danger" bsSize="large" className="mainPageBtn">Create fund</Button> */}
        {/* </Link> */}
        {/* </div> */}
        {/* <div className="mainPageBtnBlock"> */}
        {/* <Link to="/"> */}
        {/* <Button bsStyle="danger" bsSize="large" className="mainPageBtn">Become investor</Button> */}
        {/* </Link> */}
        {/* </div> */}
        {/* </div> */}

        {/* <div className="mainPageSearch"> */}
        {/* <div style={{textAlign: "center"}}> */}
        {/* <h1 className="mainPageSearchH1">Search for a Pools, Funds</h1> */}
        {/* <p className="mainPageSearchP">Looking for a walker or team to join or donate towards?</p> */}
        {/* <p className="mainPageSearchP">Put your money where your heart is.</p> */}
        {/* <p className="mainPageSearchP">Help create big science, like science that created the artificial */}
        {/* heart valve. Your donation will help scientists develop lifesaving breakthroughs. What will */}
        {/* you help us innovate next?</p> */}
        {/* <p className="mainPageSearchP">Simply enter their first name, last name or team name below and */}
        {/* click search.</p> */}
        {/* </div> */}
        {/* <div className="mainSeachBlockTabs"> */}
        {/* <TabContent id="uncontrolled-tab-example"> */}
        {/* <TabPane title="Pools"> */}
        {/* <Form inline> */}
        {/* <FormGroup controlId="formInlineName" bsSize="large"> */}
        {/* <Input type="text" placeholder="Pool name"/> */}
        {/* <span><b>  or </b></span> */}
        {/* <Input type="text" placeholder="Pool address"/> */}
        {/* </FormGroup>{' '} */}
        {/* <Button className="border-radius-unset" bsStyle="danger" bsSize="large" */}
        {/* type="submit">Search</Button> */}
        {/* </Form> */}
        {/* </TabPane> */}
        {/* <TabPane title="Funds"> */}
        {/* <Form inline> */}
        {/* <FormGroup controlId="formInlineName" bsSize="large"> */}
        {/* <Input type="text" placeholder="Enter fund name"/> */}
        {/* </FormGroup>{' '} */}
        {/* <Button className="border-radius-unset" bsStyle="danger" bsSize="large" */}
        {/* type="submit">Search</Button> */}
        {/* </Form> */}
        {/* </TabPane> */}
        {/* </TabContent> */}
        {/* </div> */}
        {/* </div> */}
        </main>
        <Footer/>
      </div>
    )
  }
}

export default (withStyles(s)(WelcomePage));
