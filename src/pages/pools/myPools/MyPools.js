import React, {Component} from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import cx from 'classnames';
import PropTypes from "prop-types";
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {
  Row,
  Col,
  Table,
  Progress,
  Badge,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap';

import Widget from '../../../components/Widget/Widget';
import s from '../poolList/Static.scss';
import {getOwnerPools} from "../../../actions/pools";
import factory from "../../../ethereum/factory";
import web3 from "../../../ethereum/web3";
import Fund from "../../../ethereum/build/Fund.json";

class MyPools extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    ownerPools: PropTypes.array, // eslint-disable-line
    isFetching: PropTypes.bool,
    login: PropTypes.string
  };

  static defaultProps = {
    isFetching: true
  };

  static meta = {
    title: 'Pools list',
    description: 'About description',
  };

  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: "1",
      poolStatus: "",
      poolBalance: "",
      poolName: '',
      startDate: '',
      id: '',
      deployedPoolAddress: '',
      ethLoading: true
    }
  }

  componentWillMount() {
    console.log("login:::", this.props.login)
    this.props.dispatch(getOwnerPools(this.props.login));
    // this.getGenearalPoolInfo()
  }

  componentDidMount() {
    // let poolInfo = ''
    // if (this.props.pools) {
    //   poolInfo = this.getGenearalPoolInfo(this.props.pools[0].deployedPoolAddress)
    // }
  }

  getFundBalance = async (fundAddress) => {
    // try {
    //   let fundInfo = ''
    const sleep = (milliseconds) => {
      return new Promise(resolve => setTimeout(resolve, milliseconds))
    }
    const instance = this.getFundAtAddress(fundAddress)
    const balance = await instance.methods.poolBalance().call()
    await sleep(1000);
    console.log("getFundBalance:::", balance)
    // if (status && balance) {
    //   this.setState({poolBalance: balance, poolStatus: status, ethLoading: false})
    //   fundInfo = {status, balance}
    //   return fundInfo
    // }
    return balance
    // } catch (err) {
    //   console.log(err)
    //   return ''
    // }
  }

  getFundAtAddress = (fundAddress) => {
    try {
      if (fundAddress) {
        const instance = new web3.eth.Contract(
          Fund.abi,
          `${fundAddress}`
        );
        return instance
      } else
        throw new Error("No fund address ", fundAddress);
    } catch (e) {
      console.log(e)
      return ''
    }
  }

  getFundStatus = async (fundAddress) => {
    const instance = this.getFundAtAddress(fundAddress)
    const status = await instance.methods.poolStatus().call()
    return status
  }

  parsePoolList() {
    return this.props.ownerPools.map((pool) => (
      <tr key={pool.id}>
        <td><Link to={`/app/pools/${pool.id}`}>{pool.poolName}</Link></td>
        <td>{pool.deployedPoolAddress}</td>
        {/*<td>{this.getFundBalance(pool.deployedPoolAddress)}</td>*/}
        {/*<td>{this.getFundStatus(pool.deployedPoolAddress)}</td>*/}
        <td>{pool.startDate}</td>
      </tr>
    ))
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  render() {
    console.log("new render ::::::::::::::::: here you are")
    return (
      <div>
        <Row>
          <Col sm={12}>
            <Widget
              title={<h5>Pool <span className="fw-semi-bold">List</span></h5>} settings close
            >
              <div className="table-responsive">
                {/*<TabContent activeTab={this.state.activeTab}>*/}
                {/*<TabPane tabId="1">*/}
                <Table className="table-hover">
                  <thead>
                  <tr>
                    <th>Pool name</th>
                    <th>Contract address</th>
                    {/*<th>Status</th>*/}
                    {/*<th>Balance</th>*/}
                    <th>Start date</th>
                  </tr>
                  </thead>
                  <tbody>
                  {this.props.ownerPools && (
                    this.parsePoolList()
                  )}
                  {/*<tr>*/}
                  {/*<td><Link to={`/app/pools/${this.props.pools[0].id}`}>{this.props.pools[0].poolName}</Link></td>*/}
                  {/*<td>{this.props.pools[0].deployedPoolAddress}</td>*/}
                  {/*<td>{this.state.poolStatus}</td>*/}
                  {/*<td>{this.state.poolBalance}</td>*/}
                  {/*<td>{this.props.pools[0].startDate}</td>*/}
                  {/*</tr>*/}

                  {/*{(this.props.pools && !this.state.poolStatus && !this.state.poolBalance) && (*/}
                  {/*<tr>*/}
                  {/*<td><Link to={`/app/pools/${this.props.pools[0].id}`}>{this.props.pools[0].poolName}</Link></td>*/}
                  {/*<td>{this.props.pools[0].deployedPoolAddress}</td>*/}
                  {/*<td>not loaded yet</td>*/}
                  {/*<td>not loaded yet</td>*/}
                  {/*<td>{this.props.pools[0].startDate}</td>*/}
                  {/*</tr>*/}
                  {/*)}*/}
                  {this.props.ownerPools &&
                  !this.props.ownerPools.length && (
                    <tr>
                      <td colSpan="100">No pools yet</td>
                    </tr>
                  )}
                  {this.props.isFetching && (
                    <tr>
                      <td colSpan="100">Loading...</td>
                    </tr>
                  )}
                  </tbody>
                </Table>
                {/*</TabPane>*/}
                {/*</TabContent>*/}
              </div>
            </Widget>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  console.log("STATE::", state)
  return {
    isFetching: state.pools.isFetching,
    login: state.user.login,
    ownerPools: state.pools.ownerPools
  };
}

export default connect(mapStateToProps)(withStyles(s)(MyPools));
