import React from 'react';
import { Switch, Route, withRouter } from 'react-router';
import PoolsTable from "./poolList/PoolsTable";
import PoolInfo from "./poolInfo/PoolInfo";
import CreatePool from "./new/CreatePool";
import MyPools from "./myPools/MyPools";

class Pools extends React.Component {
  render() {
    return (
      <Switch>
        <Route path="/app/pools" exact component={PoolsTable} />
        <Route path="/app/pools/create" exact component={CreatePool} />
        <Route path="/app/pools/myPools" exact component={MyPools} />
        <Route path="/app/pools/:id" exact component={PoolInfo} />
      </Switch>
    );
  }
}

export default withRouter(Pools);
