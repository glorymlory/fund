import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import cx from 'classnames';
import {
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Button,
  ButtonGroup,
  Alert,
  Label,
  Breadcrumb,
  BreadcrumbItem,
  Nav,
  NavItem,
  NavLink,
  Media,
  Progress,
  Table,
  Modal,
  ModalBody,
  ModalFooter, InputGroup, InputGroupAddon, FormFeedback, ModalHeader, Spinner
} from 'reactstrap';
import {connect} from 'react-redux';

import withMeta from '../../../core/withMeta';
import Widget from '../../../components/Widget';

import {createPost} from '../../../actions/posts';
import s from './PoolInfo.scss';
import {Link} from "react-router-dom";
import projPhoto from "../../../images/ameal2.jpg";
import {getPool} from "../../../actions/pools";
import tgIcon from "../../../images/telegramIcon.png";
import web3 from "../../../ethereum/web3";
import factory from "../../../ethereum/factory";
import Fund from "../../../ethereum/build/Fund.json";
import WAValidator from "wallet-address-validator";
import history from "../../../history";

class PoolInfo extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    message: PropTypes.string,
    isFetching: PropTypes.bool,
    pool: PropTypes.object
  };

  static defaultProps = {
    isFetching: false,
    message: null,
  };

  static meta = {
    title: 'Create new post',
    description: 'About description',
  };

  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      unmountOnClose: true,
      contrAmount: 0,
      contrAmountError: false,
      contributing: false,
      errorMessage: '',
      backdrop: true,
      modalErr: false,
      poolBalance: '',
      poolContributors: '',
      poolStatus: 9,
      contributed: false
    };

    this.toggle = this.toggle.bind(this);
    this.toggleSend = this.toggleSend.bind(this);
    this.changeUnmountOnClose = this.changeUnmountOnClose.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.getFundInfo = this.getFundInfo.bind(this);
  }

  componentWillMount() {
    this.props.dispatch(getPool(this.props.match.params.id));
    if (this.props.pool)
      console.log(this.props.pool.poolName)
  }

  componentDidMount() {
    this.getFundInfo()
  }

  toggleModal() {
    this.setState(prevState => ({
      modalErr: !prevState.modalErr
    }));
  }

  parsePool() {
    return this.props.pool((value) => (
      <td>{value}</td>
    ))
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal,
    }));
  }

  changeUnmountOnClose(e) {
    let value = e.target.value;
    this.setState({unmountOnClose: JSON.parse(value)});
  }

  changeContrAmount = (event) => {
    if (event.target.value < 0 || !event.target.value) {
      this.setState({contrAmount: event.target.value, contrAmountError: true})
    } else
      this.setState({contrAmount: event.target.value, contrAmountError: false})
  }

  getFundAtAddress = (fundAddress) => {
    try {
      if (fundAddress) {
        const instance = new web3.eth.Contract(
          Fund.abi,
          `${fundAddress}`);
        return instance
      } else
        throw new Error("No fund address ", fundAddress);
    } catch (e) {
      console.log(e)
      return ''
    }
  }

  getTransactionReceiptMined(txHash, interval) {
    const self = this;
    const transactionReceiptAsync = function (resolve, reject) {
      web3.eth.getTransactionReceipt(txHash, (error, receipt) => {
        if (error) {
          reject("getTransactionReceipt::::", error);
        } else if (receipt == null) {
          setTimeout(
            () => transactionReceiptAsync(resolve, reject),
            interval || 2000);
        } else {
          resolve(receipt);
        }
      });
    };

    if (Array.isArray(txHash)) {
      return Promise.all(txHash.map(
        oneTxHash => self.getTransactionReceiptMined(oneTxHash, interval)));
    } else if (typeof txHash === "string") {
      return new Promise(transactionReceiptAsync);
    }
    throw new Error("Invalid Type: ", txHash);

  };

  async toggleSend() {
    // e.preventDefault()
    try {
      if (!this.state.contrAmountError &&
        WAValidator.validate(this.props.pool.deployedPoolAddress, 'ETH')) {
        const accounts = await web3.eth.getAccounts();
        const account = accounts[0];
        if (!account) {
          this.toggleModal()
          throw new Error('Ooops!');
        }

        this.setState({contributing: true, backdrop: "static"})
        const trand = web3.eth.sendTransaction({
          from: accounts[0],
          to: this.props.pool.deployedPoolAddress,
          value: web3.utils.toWei(this.state.contrAmount, 'ether')
        }, async (err, transactionId) => {
          try {
            if (err) {
              throw (err)
            }
            await this.getTransactionReceiptMined(transactionId, 3000)
            this.setState({
              contributing: false,
              contributed: true
            })
          } catch (error) {
            this.setState({contributing: false, errorMessage: error.message})
            console.log("problem:::::", error)
          }
        });
      }
    } catch (err) {
      this.setState({contributing: false, errorMessage: err.message})
      console.log("HELL NO::::", err)
    }
  }

  refreshPage() {
    window.location.reload();
  }

  async getFundInfo() {
    const sleep = (milliseconds) => {
      return new Promise(resolve => setTimeout(resolve, milliseconds))
    }
    await sleep(2000);
    const instance = this.getFundAtAddress(this.props.pool.deployedPoolAddress)
    const balance = await instance.methods.poolBalance().call()
    const poolStatus = await instance.methods.poolStatus().call()
    if (balance) {
      this.setState({poolBalance: balance, poolContributors: 10, poolStatus: poolStatus})
      console.log("instance:::", balance, poolStatus)
      return true
    } else
      return false
  }

  render() {
    let acceptsCurr = ''
    let countCurr = 0
    if (this.props.pool) {
      if (this.props.pool.currencyEth) {
        acceptsCurr += "ETH";
        countCurr++;
      }
      if (this.props.pool.currencyDollar) {
        if (countCurr >= 1)
          acceptsCurr += ",";
        acceptsCurr += " USD";
      }
      if (this.props.pool.currencyGo) {
        if (countCurr >= 1)
          acceptsCurr += ",";
        acceptsCurr += " GO";
      }
    }

    return (
      <div className={s.root}>
        <Breadcrumb>
          <BreadcrumbItem>
            <Link to="/app/pools">
              Find pool
            </Link>
          </BreadcrumbItem>
          <BreadcrumbItem active>Pool info</BreadcrumbItem>
        </Breadcrumb>
        <h1>Pool Info</h1>
        <Row>
          <Col sm={12}>
            <Widget>
              <Table responsive borderless className={cx('mb-0', s.usersTable)}>
                <thead>
                <tr>
                  <th>Pool Name</th>
                  <th>Private</th>
                  <th>Status</th>
                  <th>Starting date</th>
                  <th>Ending date</th>
                  <th>Hard Cap</th>
                  <th>Bonus</th>
                  <th>Auto dist.</th>
                  <th>KYC</th>
                  <th>Max per contr.</th>
                  <th>Min per contr.</th>
                  <th>Participation Restrictions</th>
                  <th>Accepts</th>
                </tr>
                </thead>
                <tbody>
                {(this.props.pool) && (
                  <tr>
                    <td>{this.props.pool.poolName}</td>
                    {this.props.pool.privatePool && (
                      <td>Yes</td>
                    )}
                    {!this.props.pool.privatePool && (
                      <td>No</td>
                    )}
                    {parseInt(this.state.poolStatus, 10) === 9 && (
                      <td><span className="py-0 px-1 bg-warning rounded text-white">pending</span></td>
                    )}
                    {(parseInt(this.state.poolStatus, 10) === 1 && this.state.poolStatus) && (
                      <td><span className="py-0 px-1 bg-success rounded text-white">active</span></td>
                    )
                    }
                    {(parseInt(this.state.poolStatus, 10) !== 1 && parseInt(this.state.poolStatus, 10) !== 9) && (
                      <td><span className="py-0 px-1 bg-danger rounded text-white">paused</span></td>
                    )
                    }
                    <td>{this.props.pool.startDate}</td>
                    <td>{this.props.pool.endDate}</td>
                    <td>{this.props.pool.totalAmount}</td>
                    <td>{this.props.pool.bonus}</td>
                    {this.props.pool.distributeCheckbox && (
                      <td>dd</td>
                    )}
                    {!this.props.pool.distributeCheckbox && (
                      <td>dd</td>
                    )}
                    {this.props.pool.KYCCheckbox && (
                      <td>dd</td>
                    )}
                    {!this.props.pool.KYCCheckbox && (
                      <td>dd</td>
                    )}
                    <td>{this.props.pool.maxPerContr}</td>
                    <td>{this.props.pool.minPerContr}</td>
                    <td>{this.props.pool.contributorsRestrictions}</td>
                    <td>{acceptsCurr}</td>
                  </tr>
                )}
                </tbody>
              </Table>
            </Widget>
          </Col>
        </Row>
        <Row>
          <Col sm={12} md={7}>
            {this.props.pool && (
              <Widget>
                <h2>{this.props.pool.poolName}</h2>
                <p>Volentix introduces a decentralized digital assets exchange connected with a secure multi-currency
                  cross-blockchain peer-to-peer wallet, a user-friendly market-ratings analytical interface, and an
                  incentives-based recruitment program</p>
                <p>VENUE</p>
                <p>Venue is planned as a dynamic community platform that recruits and aligns members of the Volentix
                  community to facilitate distribution of VTX, the native digital asset of the Volentix ecosystem, and
                  to
                  promote awareness of Volentix initiatives. Recently launched in beta testing, Venue enables users to
                  receive VTX in exchange, for example, for participating in developing dedicated communities,
                  submitting
                  bug fixes, and claiming bounties. Leaderboards and live metrics reflect user participation. The first
                  signature campaign was launched on the https:// bitcointalk.org/ forum on July 13, 2018. Please visit
                  https://venue.volentix.io for more information </p>
                <p>VERTO</p>
                <p>Verto is being built as a multi-currency wallet for use with the VDex decentralized exchange and
                  intends to facilitate personal custody and local management of private and public keys in peer-to-peer
                  transactions, with the goal of eliminating the risks of devastating losses of stake associated with
                  traumatic failures of central operators. Verto plans to employ a system of smart contracts to maintain
                  the state between two trading clients, the simplest operations being accomplished with atomic
                  swaps.[1]</p>
                <p>VESPUCCI</p>
                <p>Vespucci is envisioned as an analytics engine accessible via a user-friendly interface with treasure
                  troves of real time and historical market data, such as digital assets ratings and sentiment analyses.
                  We wish to empower users with tools to graph and compare tradeable digital assets, to access and parse
                  historical trading records, to plot trends and patterns, and to monitor and assess open-source
                  software
                  developments. Vespucci seeks to bring to your fingertips confident and comprehensive market-relevant
                  data by aggregating the information currently scattered throughout many different blockchains,
                  websites,
                  chat rooms, and exchanges.</p>
                <p>VDEX</p>
                <p>The fourth pillar of Volentix, the VDex exchange, is the tradable digital assets platform introduced
                  in
                  detail in this white paper. For smooth and secure usability, we plan VDex to integrate with your own
                  personal Verto wallet and Vespucci interface. We expect VDex to be able to manage transactions
                  involving
                  both VTX and the vast array of digital assets and blockchains extant from time to time throughout the
                  world. We are developing Venue as a complementary adjunct primarily in order to incentivize and drive
                  native VTX-based initiatives.</p>
              </Widget>
            )}
          </Col>
          <Col sm={12} md={5}>
            <Widget
              title={
                <div className="mb-lg-3">
                <span className="text-uppercase font-weight-bold ">
                 Current<span className="fw-semi-bold"> status</span>
                </span>
                </div>
              }>
              <Row>
                <img src={projPhoto} className="img-thumbnail w-50 h-50 mx-auto d-block"/>
              </Row>
              <Row className="m-lg-3">
                <Col sm={12}>
                  {(this.state.poolBalance && this.props.pool.totalAmount) && (
                    <div>
                      <span className="fw-semi-bold">Collected ethereum</span>
                      <div
                        className="text-center">{web3.utils.fromWei(this.state.poolBalance, 'ether')}/{this.props.pool.totalAmount}</div>
                      <Progress className="progress-sm mx-auto m-lg-1" color="success"
                                value={web3.utils.fromWei(this.state.poolBalance, 'ether')}
                                max={this.props.pool.totalAmount}/>
                      <br/>
                      {/*<span className="fw-semi-bold">Contributors</span>*/}
                      {/*<div className="text-center">{this.state.poolContributors}/50</div>*/}
                      {/*<Progress className="progress-sm mx-auto m-lg-1" color="success" value={this.state.poolContributors} max={50}/>*/}
                    </div>
                  )}
                </Col>
              </Row>
              <Row className="m-lg-5 mx-auto">
                <Button color="primary" size="lg" onClick={this.toggle} block
                        className={cx("mb-xs mr-xs", s.buttonSpec)}>Contribute</Button>
              </Row>
              <Modal isOpen={this.state.modal} toggle={this.toggle}
                     className={this.props.className} centered
                     backdrop={this.state.backdrop}>
                {(this.state.contributing && !this.state.errorMessage && !this.state.contributed) &&
                <div className="col-xs-1 text-center">
                <ModalBody>
                <Spinner style={{width: '3rem', height: '3rem'}}/>{''}
                <p>{this.state.errorMessage}</p>
                </ModalBody>
                </div>
                }
                {(!this.state.contributing && !this.state.errorMessage && !this.state.contributed) && (
                <div>
                <ModalHeader toggle={this.toggle}>Modal title</ModalHeader>
                <ModalBody>
                <Label for="netAmount">Amount to contribute :</Label>
                <InputGroup>
                <Input type="number" name="netAmount" id="netAmount" placeholder="1000"
                value={this.state.contrAmount}
                onChange={this.changeContrAmount}
                required
                min={0}
                invalid={this.state.contrAmountError}/>
                <InputGroupAddon addonType="append">ETH</InputGroupAddon>
                </InputGroup>
                <FormFeedback tooltip>Put value from 1 to 100000</FormFeedback>
                </ModalBody>
                <ModalFooter>
                <Button color="primary" onClick={this.toggleSend}
                disabled={this.state.contrAmountError}>Send</Button>
                </ModalFooter>
                </div>
                )}
                {this.state.errorMessage && (
                <div>
                <ModalHeader toggle={this.toggle}>Error occurred</ModalHeader>
                <ModalBody>
                <p>{this.state.errorMessage}</p>
                <p>Please contact us!</p>
                </ModalBody>
                <ModalFooter>
                <a href="https://t.me/uxidesign">
                Support
                </a>
                <Button color="secondary" onClick={this.refreshPage}>Refresh page</Button>
                </ModalFooter>
                </div>
                )}
                {(this.state.contributed && !this.state.errorMessage && !this.state.contributing) && (
                  <div>
                      <ModalHeader toggle={this.toggle}>Contributed!</ModalHeader>
                    <ModalBody>
                      <p>
                        Well, you cant track your ROI and many more on dashboard.
                        Join our chat! (here supposed to be telegram link)

                        Subscribe to platform telegram channel to get last updates and news.
                      </p>
                      <hr/>
                      <p className="mb-0 text-right">“You have to spend money to make money.”</p>
                    </ModalBody>
                    <ModalFooter>
                      <Button color="secondary" onClick={() => {
                        history.push("/app")
                      }}>Back to dashboard</Button>
                    </ModalFooter>
                  </div>
                )}
              </Modal>
              <Modal isOpen={this.state.modalErr} toggle={this.toggleModal}>
                <ModalHeader toggle={this.toggleModal}>Metamask account problem</ModalHeader>
                <ModalBody>
                  Probably you dont have metamask or its troubles with your account. Please contact support.
                  <a href="https://t.me/glorymlory">telegram chat</a>
                </ModalBody>
                <ModalFooter>
                  <Button color="secondary" onClick={this.toggleModal}>Ok</Button>
                </ModalFooter>
              </Modal>
            </Widget>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    pool: state.pools.pool,
    isFetching: state.pools.isFetching,
    message: state.pools.message,
  };
}

export default connect(mapStateToProps)(withStyles(s)(withMeta(PoolInfo)));
