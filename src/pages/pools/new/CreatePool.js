import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Button,
  ButtonGroup,
  Alert,
  Label,
  Breadcrumb,
  BreadcrumbItem,
} from 'reactstrap';
import {connect} from 'react-redux';

import withMeta from '../../../core/withMeta';
import Widget from '../../../components/Widget';

import {createPost} from '../../../actions/posts';
import s from './CreatePool.scss';
import StepOne from "./createPages/StepOne";

class CreatePool extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    message: PropTypes.string,
    isFetching: PropTypes.bool,
  };

  static defaultProps = {
    isFetching: false,
    message: null,
  };

  static meta = {
    title: 'Create new post',
    description: 'About description',
  };

  constructor(props) {
    super(props);

    this.state = {
      title: '',
      content: '',
    };
  }

  changeTitle = (event) => {
    this.setState({title: event.target.value});
  }

  changeContent = (event) => {
    this.setState({content: event.target.value});
  }

  doCreatePost = (e) => {
    this.props
      .dispatch(
        createPost({
          title: this.state.title,
          content: this.state.content,
        }),
      )
      .then(() =>
        this.setState({
          title: '',
          content: '',
        }),
      );
    e.preventDefault();
  }

  render() {
    return (
      <div className={s.root}>
        <Breadcrumb>
          <BreadcrumbItem>YOU ARE HERE</BreadcrumbItem>
          <BreadcrumbItem active>New Pool</BreadcrumbItem>
        </Breadcrumb>
        <h1>Create A Pool</h1>
          {/* <div className="mx-auto w-50"> */}
          <StepOne/>
          {/* </div> */}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isFetching: state.posts.isFetching,
    message: state.posts.message,
  };
}

export default connect(mapStateToProps)(withStyles(s)(withMeta(CreatePool)));
