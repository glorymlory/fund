import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Button,
  ButtonGroup,
  Alert,
  Label,
  Popover,
  PopoverHeader,
  PopoverBody,
  CustomInput,
  InputGroup,
  InputGroupAddon,
  UncontrolledTooltip,
  Spinner,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormFeedback,
  Jumbotron, BreadcrumbItem
} from 'reactstrap';
import WAValidator from 'wallet-address-validator';
import factory from '../../../../ethereum/factory';
import web3 from "../../../../ethereum/web3";
import withMeta from '../../../../core/withMeta';
import Widget from '../../../../components/Widget';
import {createPool} from "../../../../actions/pools";

import s from './PoolStepCreation.scss';
import history from "../../../../history";
import {Link} from "react-router-dom";

class StepOne extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    message: PropTypes.string,
    isFetching: PropTypes.bool,
    login: PropTypes.string
  };

  static defaultProps = {
    isFetching: false,
    message: null,
  };

  static meta = {
    title: 'Create new post',
    description: 'About description',
  };

  constructor(props) {
    super(props);
    this.toggleAllocation = this.toggleAllocation.bind(this);
    this.onChangeDistributeCheckbox = this.onChangeDistributeCheckbox.bind(this);
    this.onChangeExtraAdminsCheckbox = this.onChangeExtraAdminsCheckbox.bind(this);
    this.onChangeWhitelistCheckbox = this.onChangeWhitelistCheckbox.bind(this);
    this.onChangeLockAdrCheckbox = this.onChangeLockAdrCheckbox.bind(this);
    this.onChangeKYCCheckbox = this.onChangeKYCCheckbox.bind(this);
    this.toggleModal = this.toggleModal.bind(this);

    this.state = {
      id: "",
      poolName: '',
      popoverOpenAllocation: false,
      cSelectedOne: [1],
      ownerWalletAddress: "",
      netAmount: "",
      ownerFee: 5,
      totalAmount: 0,
      maxPerContr: 50,
      minPerContr: "0.5",
      distributeCheckbox: true,
      extraAdminsCheckbox: false,
      whitelistCheckbox: false,
      lockAdrCheckbox: false,
      KYCCheckbox: false,
      errorMessage: "",
      deploying: false,
      modal: false,
      formErrors: {ownerWalletAddress: false, netAmount: false, ownerFee: false},
      poolNameValid: false,
      poolCreated: false,
      deployedPoolAddress: '',
      login: ''
    };
  }

  toggleModal() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  toggleAllocation() {
    this.setState({
      popoverOpenAllocation: !this.state.popoverOpenAllocation
    });
  }

  changeOwnerWalletAddress = (event) => {
    if (event.target.value) {
      if (WAValidator.validate(event.target.value, 'ETH')) {
        this.setState({ownerWalletAddress: event.target.value});
        this.setState({
          formErrors: {
            ownerWalletAddress: false,
            netAmount: this.state.formErrors.netAmount,
            ownerFee: this.state.formErrors.ownerFee
          }
        })
      } else {
        this.setState({ownerWalletAddress: event.target.value});
        this.setState({
          formErrors: {
            ownerWalletAddress: true,
            netAmount: this.state.formErrors.netAmount,
            ownerFee: this.state.formErrors.ownerFee
          }
        })
      }
    } else {
      this.setState({ownerWalletAddress: event.target.value});
      this.setState({
        formErrors: {
          ownerWalletAddress: true,
          netAmount: this.state.formErrors.netAmount,
          ownerFee: this.state.formErrors.ownerFee
        }
      })
    }
  }

  changeNetAmount = (event) => {
    if (event.target.value < 0 || !event.target.value) {
      this.setState(
        {netAmount: event.target.value}
      );
      this.setState({
        formErrors: {
          ownerWalletAddress: this.state.formErrors.ownerWalletAddress,
          netAmount: true,
          ownerFee: this.state.formErrors.ownerFee
        }
      })
    } else {
      this.setState({
        formErrors: {
          ownerWalletAddress: this.state.formErrors.ownerWalletAddress,
          netAmount: false,
          ownerFee: this.state.formErrors.ownerFee
        }
      })
      const totalAmountCurr = parseInt(((this.state.ownerFee * event.target.value) / 100), 10) + parseInt(event.target.value, 10)
      this.setState(
        {netAmount: event.target.value, totalAmount: totalAmountCurr}
      );
    }
  }

  changeOwnerFee = (event) => {
    if (event.target.value < 0 || event.target.value > 100 || !event.target.value) {
      this.setState({ownerFee: event.target.value});
      this.setState({
        formErrors: {
          ownerWalletAddress: this.state.formErrors.ownerWalletAddress,
          netAmount: this.state.formErrors.netAmount,
          ownerFee: true
        }
      })
    } else {
      this.setState({
        formErrors: {
          ownerWalletAddress: this.state.formErrors.ownerWalletAddress,
          netAmount: this.state.formErrors.netAmount,
          ownerFee: false
        }
      })
      const totalAmountCurr = parseInt(((this.state.netAmount * event.target.value) / 100), 10) + parseInt(this.state.netAmount, 10)
      this.setState({ownerFee: event.target.value, totalAmount: totalAmountCurr});
    }

  }

  changePoolName = (event) => {
    if (event.target.value) {
      this.setState({poolName: event.target.value, poolNameValid: false});
    } else {
      this.setState({poolName: event.target.value, poolNameValid: true})
    }
  }

  changeTotalAmount = (event) => {
    this.setState({totalAmount: event.target.value});
  }

  changeMinPerContr = (event) => {
    this.setState({minPerContr: event.target.value});
  }

  changeMaxPerContr = (event) => {
    this.setState({maxPerContr: event.target.value});
  }

  onCheckboxBtnClickOne(selected) {
    const index = this.state.cSelectedOne.indexOf(selected);
    if (index < 0) {
      this.state.cSelectedOne.push(selected);
    } else {
      this.state.cSelectedOne.splice(index, 1);
    }
    this.setState({cSelectedOne: [...this.state.cSelectedOne]});
  }

  onChangeDistributeCheckbox() {
    this.setState({distributeCheckbox: !this.state.distributeCheckbox});
  }

  onChangeExtraAdminsCheckbox() {
    this.setState({extraAdminsCheckbox: !this.state.extraAdminsCheckbox});
  }

  onChangeWhitelistCheckbox() {
    this.setState({whitelistCheckbox: !this.state.whitelistCheckbox});
  }

  onChangeLockAdrCheckbox() {
    this.setState({lockAdrCheckbox: !this.state.lockAdrCheckbox});
  }

  onChangeKYCCheckbox() {
    this.setState({KYCCheckbox: !this.state.KYCCheckbox});
  }

  getTransactionReceiptMined(txHash, interval) {
    const self = this;
    const transactionReceiptAsync = function (resolve, reject) {
      web3.eth.getTransactionReceipt(txHash, (error, receipt) => {
        if (error) {
          reject("getTransactionReceipt::::", error);
        } else if (receipt == null) {
          setTimeout(
            () => transactionReceiptAsync(resolve, reject),
            interval || 2000);
        } else {
          resolve(receipt);
        }
      });
    };

    if (Array.isArray(txHash)) {
      return Promise.all(txHash.map(
        oneTxHash => self.getTransactionReceiptMined(oneTxHash, interval)));
    } else if (typeof txHash === "string") {
      return new Promise(transactionReceiptAsync);
    }
    throw new Error("Invalid Type: ", txHash);

  };

  validateNetAmount() {
    if (this.state.netAmount < 0 || !this.state.netAmount) {
      this.setState({
        formErrors: {
          ownerWalletAddress: this.state.formErrors.ownerWalletAddress,
          netAmount: true,
          ownerFee: this.state.formErrors.ownerFee
        }
      })
      return false
    }
    this.setState({
      formErrors: {
        ownerWalletAddress: this.state.formErrors.ownerWalletAddress,
        netAmount: false,
        ownerFee: this.state.formErrors.ownerFee
      }
    })
    return true

  }

  validateOwnerFee() {
    if (this.state.ownerFee < 0 || this.state.ownerFee > 100 || !this.state.ownerFee) {
      this.setState({
        formErrors: {
          ownerWalletAddress: this.state.formErrors.ownerWalletAddress,
          netAmount: this.state.formErrors.netAmount,
          ownerFee: true
        }
      })
      return false
    }
    this.setState({
      formErrors: {
        ownerWalletAddress: this.state.formErrors.ownerWalletAddress,
        netAmount: this.state.formErrors.netAmount,
        ownerFee: false
      }
    })
    return true

  }

  validateCurrency() {
    if (this.state.cSelectedOne.includes(1) || this.state.cSelectedOne.includes(2) || this.state.cSelectedOne.includes(3))
      return true

    return false
  }

  validateOwnerAddress() {
    if (this.state.ownerWalletAddress) {
      if (WAValidator.validate(this.state.ownerWalletAddress, 'ETH')) {
        this.setState({
          formErrors: {
            ownerWalletAddress: false,
            netAmount: this.state.formErrors.netAmount,
            ownerFee: this.state.formErrors.ownerFee
          }
        })
        return true
      }
      this.setState({
        formErrors: {
          ownerWalletAddress: true,
          netAmount: this.state.formErrors.netAmount,
          ownerFee: this.state.formErrors.ownerFee
        }
      })
      return false
    }
    this.setState({
      formErrors: {
        ownerWalletAddress: true,
        netAmount: this.state.formErrors.netAmount,
        ownerFee: this.state.formErrors.ownerFee
      }
    })
    return false
  }

  poolNameValid() {
    const val = this.state.poolName;
    if (val && val.length > 3 && val.length < 100)
      return true
    return false
  }

  validateForm() {
    if (this.validateOwnerAddress() && this.validateCurrency() &&
      this.validateNetAmount() && this.validateOwnerFee() &&
      this.poolNameValid()) {
      return true;
    }
    throw new Error('Invalid form input!')
  }

  async getLastDeployedPool() {
    try {
      if (true) {
        // const appContract = web3.eth.contract(appContractAbi).at(app_contract_address);
        // const accounts = await web3.eth.getAccounts();
        // const account = accounts[0];
        // if (!account) {
        //   this.toggleModal()
        //   throw new Error('Ooops!');
        // }
        const fundAddress = await factory.methods.lastDeployedFund().call()
        console.log("fundAddress:::", fundAddress)
      }
    } catch (err) {
      this.setState({deploying: false, errorMessage: err.message})
    }
  }

  onSubmit = async (e) => {
    e.preventDefault()
    try {
      if (this.validateForm()) {
        // const appContract = web3.eth.contract(appContractAbi).at(app_contract_address);
      //   ethereum.enable().then((account) =>{
      //      const defaultAccount = account[0]
      //      console.log(defaultAccount);

      //  })
        const accounts = await web3.eth.getAccounts();
        console.log(accounts[0]);
        const account = accounts[0];
        if (!account) {
          this.toggleModal()
          throw new Error('Ooops!');
        }
        this.setState({deploying: true})

        // const newFactory = await new web3.eth.Contract(
        //   JSON.parse(PoolFactory.interface)
        // ).deploy({ data: PoolFactory.bytecode })
        //   .send({ gas: '1000000', from: accounts[0] });
      
        // console.log('Contract deployed to', newFactory.options.address);
        // console.log("hello" + newFactory)
        
        const newPool = await factory.methods.deployPool(
          this.state.ownerWalletAddress,
          web3.utils.toWei(`${this.state.totalAmount}`, 'ether'),
          this.state.ownerFee
        ).send({
          from: accounts[0]
        }, async (err, transactionId) => {
          if (err) {
            throw(err)
          } else {
            console.log("ELSE::::::", transactionId, "::::", newPool)
            try {
              const sleep = (milliseconds) => {
                return new Promise(resolve => setTimeout(resolve, milliseconds))
              }
              await this.getTransactionReceiptMined(transactionId, 6000)
              await sleep(1000);
              console.log("alles gut:::: here is pool address", newPool)
              const fundAddress = await factory.methods.lastDeployedFund().call()
              console.log("alles gut:::: here is pool address", fundAddress)
              this.setState({
                deploying: false,
                poolCreated: true,
                deployedPoolAddress: fundAddress
              })
              await this.doCreatePool()
            } catch (error) {
              this.setState({deploying: false, errorMessage: error})
              console.log("problem:::::", error)
            }
          }
        });
      }
    } catch (err) {
      this.setState({deploying: false, errorMessage: err.message})
    }
  };


  genId() {
    const min = 1000;
    const max = 100000;
    const rand = Math.floor(min + Math.random() * (max - min));

    const poolname = `${this.state.poolName}`.replace(/\s/g, '');
    const idstr = `${poolname}${rand}`;
    console.log(idstr)

    this.setState({id: idstr})
  }

  doCreatePool = () => {
    // e.preventDefault()
    // await this.doPoolDeployment()
    this.genId();
    this.props
      .dispatch(
        createPool({
          id: this.state.id,
          poolName: this.state.poolName,
          deployedPoolAddress: this.state.deployedPoolAddress,
          currencyEth: this.state.cSelectedOne.includes(1),
          currencyDollar: this.state.cSelectedOne.includes(2),
          currencyGo: this.state.cSelectedOne.includes(3),
          ownerWalletAddress: this.state.ownerWalletAddress,
          ownerFee: this.state.ownerFee,
          totalAmount: this.state.totalAmount,
          maxPerContr: this.state.maxPerContr,
          minPerContr: this.state.minPerContr,
          distributeCheckbox: this.state.distributeCheckbox,
          extraAdminsCheckbox: this.state.extraAdminsCheckbox,
          whitelistCheckbox: this.state.whitelistCheckbox,
          lockAdrCheckbox: this.state.lockAdrCheckbox,
          KYCCheckbox: this.state.KYCCheckbox,
          softcap: 0,
          privatePool: false,
          startDate: new Date().toLocaleString().toString(),
          endDate: new Date().toLocaleString().toString(),
          contributorsRestrictions: "Ukraine, USA, China, Russia, UAE",
          bonus: "25",
          rating: 0,
          webRef: "https://t.me/uxidesign",
          UserId: this.props.login
        }),
      )
      .then(() =>
        this.setState({
          cSelectedOne: [1],
          deployedPoolAddress: '',
          ownerWalletAddress: '',
          netAmount: 0,
          ownerFee: 5,
          totalAmount: 0,
          maxPerContr: 50,
          minPerContr: "0.5",
          distributeCheckbox: false,
          extraAdminsCheckbox: false,
          whitelistCheckbox: false,
          lockAdrCheckbox: false,
          KYCCheckbox: false,
          deploying: false
        }),
      );
  }

  render() {
    return (
      <div>
        <Row>
          <Col sm={12} md={6}>
            <Row>
              <Col sm={12}>
                <Widget
                  title={<h5>Customize Pool<br/>
                    <hr/>
                  </h5>}>
                  <Form>
                    <FormGroup>
                      <Label for="poolName">Enter pool Name</Label>
                      <Input type="text" name="poolName" id="poolName"
                             placeholder="pool name"
                             value={this.state.poolName}
                             onChange={this.changePoolName}
                             required
                             invalid={this.state.poolNameValid}/>
                    </FormGroup>
                    <FormGroup>
                      <Label for="exampleSelect">Contribute in </Label>
                      <br/>
                      <ButtonGroup>
                        <Button
                          color="default" onClick={() => this.onCheckboxBtnClickOne(1)}
                          active={this.state.cSelectedOne.includes(1)}
                        >Ethereum</Button>
                        <Button
                          color="default" onClick={() => this.onCheckboxBtnClickOne(2)}
                          active={this.state.cSelectedOne.includes(2)}

                        >US Dollars</Button>
                        <Button
                          color="default" onClick={() => this.onCheckboxBtnClickOne(3)}
                          active={this.state.cSelectedOne.includes(3)}

                        >GO</Button>
                      </ButtonGroup>
                    </FormGroup>
                    {/* <FormGroup> */}
                    {/* <Label for="exampleSelect">Contribute in :</Label> */}
                    {/* <Input type="select" name="select" id="currencyContribute" multiple required> */}
                    {/* {contrCurrency.map(curr => ( */}
                    {/* <option disabled={!curr.visibility}>{curr.label}</option> */}
                    {/* )) */}
                    {/* } */}
                    {/* </Input> */}
                    {/* </FormGroup> */}
                    <FormGroup>
                      <Label for="ownerWalletAddress">Wallet address</Label>
                      <Input type="text" name="ownerWalletAddress" id="walletAddress"
                             placeholder="0x0000000000000000000000000000000000000000"
                             value={this.state.ownerWalletAddress}
                             onChange={this.changeOwnerWalletAddress}
                             required
                             invalid={this.state.formErrors.ownerWalletAddress}/>
                      <FormFeedback tooltip>Invalid address</FormFeedback>
                    </FormGroup>
                    {/* <FormGroup> */}
                    {/* <Label for="input-content">Content</Label> */}
                    {/* <textarea */}
                    {/* id="input-content" */}
                    {/* className="form-control" */}
                    {/* placeholder="Post Content" */}
                    {/* value={this.state.content} */}
                    {/* required */}
                    {/* onChange={this.changeContent} */}
                    {/* /> */}
                    {/* </FormGroup> */}
                  </Form>
                </Widget>
              </Col>
            </Row>
            <Row>
              <Col sm={12}>
                <Widget
                  title={
                    <h5>
                      1. Allocations <span id="Popover1" className="fw-semi-bold"
                                           style={{textDecoration: "underline", color: "black"}}>learn more</span><br/>
                      <hr/>
                      <Popover placement="bottom" isOpen={this.state.popoverOpenAllocation} target="Popover1"
                               toggle={this.toggleAllocation}>
                        <PopoverHeader>What you can</PopoverHeader>
                        <PopoverBody>You have the option to define the maximum amount your pool can raise. You can also
                          define minimum and maximum contribution thresholds for each contributor.</PopoverBody>
                      </Popover>
                    </h5>
                  }>
                  <Form>
                    <FormGroup>
                      <Label for="netAmount">Net Amount :</Label>
                      <InputGroup>
                        <Input type="number" name="netAmount" id="netAmount" placeholder="1000"
                               value={this.state.netAmount}
                               onChange={this.changeNetAmount}
                               required
                               min={0}
                               invalid={this.state.formErrors.netAmount}/>
                        <InputGroupAddon addonType="append">ETH</InputGroupAddon>
                      </InputGroup>
                      <FormFeedback tooltip>Put value from 1 to 100000</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                      <Label for="ownerFee">Your fee :</Label>
                      <InputGroup>
                        <Input type="number" name="ownerFee" id="ownerFee" placeholder="10%"
                               value={this.state.ownerFee}
                               onChange={this.changeOwnerFee}
                               required
                               min={0}
                               invalid={this.state.formErrors.ownerFee}/>
                        <InputGroupAddon addonType="append">%</InputGroupAddon>
                      </InputGroup>
                      <FormFeedback tooltip>Put value from 0 to 100</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                      <Label for="totalAmount">Total amount :</Label>
                      <InputGroup>
                        <Input type="number" name="totalAmount" id="totalAmount" disabled
                               value={this.state.totalAmount}
                               onChange={this.changeTotalAmount}
                               required
                               min={0}/>
                        <InputGroupAddon addonType="append">ETH</InputGroupAddon>
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <Label for="maxPerContr">Maximum Per Contributor :</Label>
                      <InputGroup>
                        <Input type="number" name="maxPerContr" id="maxPerContr" placeholder="50"
                               value={this.state.maxPerContr}
                               onChange={this.changeMaxPerContr}
                               required
                               min={0}
                               disabled/>
                        <InputGroupAddon addonType="append">ETH</InputGroupAddon>
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <Label for="minPerContr">Minimum Per Contributor :</Label>
                      <InputGroup>
                        <Input type="number" name="minPerContr" id="minPerContr" placeholder="0.5"
                               value={this.state.minPerContr}
                               onChange={this.changeMinPerContr}
                               required
                               min={0}
                               disabled/>
                        <InputGroupAddon addonType="append">ETH</InputGroupAddon>
                      </InputGroup>
                    </FormGroup>
                  </Form>
                </Widget>
              </Col>
            </Row>
          </Col>

          
          {this.state.deploying &&
          <Col sm={12} md={6}>
            <Widget
              title={
                <div>
                  <h5>
                    INFO
                    <hr/>
                  </h5>
                </div>
              }>
              {this.props.message && (
                <Alert className="alert-sm" color="info">
                  {this.props.message}
                </Alert>
              )}
              {this.state.deploying &&
              <div><Spinner style={{width: '3rem', height: '3rem'}}/>{''}</div>
              }

            </Widget>
          </Col>
          }
          {this.state.poolCreated &&
          <Col sm={12} md={6}>
            <Widget
              title={
                <div>
                  <h5>
                    INFO
                    <hr/>
                  </h5>
                </div>
              }>
              {this.props.message && (
                <Alert className="alert-sm" color="info">
                  {this.props.message}
                </Alert>
              )}
              <div>
                <Jumbotron>
                  <h1 className="display-3">Congratulation!</h1>
                  <p className="lead">Your pool has been created and active.</p>
                  <hr className="my-2"/>
                  <p>Now, its ready for contribution and your management. Here is your deployed fund address: {this.state.deployedPoolAddress}</p>
                  {/*<p className="lead">*/}
                    {/*<Link to={`/app/pools/${this.state.id}`}>Manage pool</Link>*/}
                  {/*</p>*/}
                  <div className="lead mt-n-xs">
                    <Link to={`/app/pools/${this.state.id}`} className="btn btn-lg btn-inverse">
                      Manage pool
                    </Link>
                  </div>
                </Jumbotron>
              </div>
            </Widget>
          </Col>
          }
        </Row>


        <Row>
          <Col sm={12} md={6}>
            <Widget
              title={
                <div>
                  <h5>
                    2. Options<br/>
                    <hr/>
                  </h5>
                  <h6>
                    Items with <i className="fa fa-lock"/> cannot be changed after pool creation.
                  </h6>
                </div>
              }>
              <Form onSubmit={this.onSubmit}>
                {this.props.message && (
                  <Alert className="alert-sm" color="info">
                    {this.props.message}
                  </Alert>
                )}
                <Row>
                  <Col sm={12} md={5}>
                    <FormGroup>
                      <div>
                        <CustomInput className="mb-2" type="checkbox" id="distributeCheckbox"
                                     label="Distribute Tokens Automatically"
                                     checked={this.state.distributeCheckbox}
                                     onChange={this.onChangeDistributeCheckbox}
                                     disabled/>
                        <CustomInput className="mb-2" type="checkbox" id="ExtraAdminsCheckbox" label="Extra Admins"
                                     checked={this.state.ExtraAdminsCheckbox}
                                     onChange={this.onChangeExtraAdminsCheckbox}
                                     disabled/>
                        <CustomInput className="mb-2" type="checkbox" id="WhitelistCheckbox" label="Create Whitelist"
                                     checked={this.state.WhitelistCheckbox}
                                     onChange={this.onChangeWhitelistCheckbox}
                                     disabled/>
                        <CustomInput className="mb-2" type="checkbox" id="lockAdrCheckbox"
                                     label="Lock Destination Address"
                                     checked={this.state.lockAdrCheckbox}
                                     onChange={this.onChangeLockAdrCheckbox}
                                     disabled/>
                        <CustomInput className="mb-2" type="checkbox" id="KYCCheckbox" label="Enable KYC"
                                     checked={this.state.KYCCheckbox}
                                     onChange={this.onChangeKYCCheckbox}
                                     disabled/>
                      </div>
                    </FormGroup>
                  </Col>
                  <Col sm={12} md={4}>
                    <div className="mb-2">
                      <span style={{textDecoration: "underline", color: "gray"}} href="#"
                            id="DistributeTip">learn more</span>
                      <UncontrolledTooltip placement="right" target="DistributeTip">
                        You can elect to have FSP automatically distribute eth to the contributors of your pool.
                        Otherwise, each of your contributors will have to claim their eth from the pool by clicking on
                        the CLAIM MY ETH button.
                      </UncontrolledTooltip>
                    </div>
                    <div className="mb-2">
                      <span style={{textDecoration: "underline", color: "gray"}} href="#"
                            id="ExtraAdminsTip">learn more  <i className="fa fa-lock"/></span>
                      <UncontrolledTooltip placement="right" target="ExtraAdminsTip">
                        You may assign other administrators who can manage the pool. These may be other people who you
                        trust or alternate wallet addresses that you control.
                      </UncontrolledTooltip>
                    </div>
                    <div className="mb-2">
                      <span style={{textDecoration: "underline", color: "gray"}} href="#"
                            id="WhitelistTip">learn more</span>
                      <UncontrolledTooltip placement="right" target="WhitelistTip">
                        You may decide whether to enforce a whitelist, in which case only contributors on the list may
                        contribute in your pool. If you click Yes, you will specify the addresses after launch. Read
                        more about whitelist gas prices in our FAQ.
                      </UncontrolledTooltip>
                    </div>
                    <div className="mb-2">
                      <span style={{textDecoration: "underline", color: "gray"}} href="#"
                            id="lockAdrTip">learn more  <i className="fa fa-lock"/></span>
                      <UncontrolledTooltip placement="right" target="lockAdrTip">
                        If you choose to lock the destination address, the contract will only allow the funds to be sent
                        to the address you enter below. The destination address will be displayed on the contributor and
                        admin dashboard.
                      </UncontrolledTooltip>
                    </div>
                    <div className="mb-2">
                      <span style={{textDecoration: "underline", color: "gray"}} href="#"
                            id="KYCTip">learn more  <i className="fa fa-lock"/></span>
                      <UncontrolledTooltip placement="right" target="KYCTip">
                        You can choose to have every contributor in your pool going through KYC/AML.
                      </UncontrolledTooltip>
                    </div>

                  </Col>
                </Row>

                <div className="d-flex justify-content-end">
                  <ButtonGroup>
                    {/* <Button color="default" disabled={this.state.deploying}>Cancel</Button> */}
                    <Button color="danger" type="submit" disabled={this.state.deploying}>
                      {this.props.isFetching ? 'Creating...' : 'Create'}
                    </Button>
                    {this.state.deploying &&
                    <div><Spinner style={{width: '3rem', height: '3rem'}}/>{''}</div>
                    }
                  </ButtonGroup>
                  <Modal isOpen={this.state.modal} toggle={this.toggle}>
                    <ModalHeader toggle={this.toggle}>Metamask account problem</ModalHeader>
                    <ModalBody>
                      Probably you dont have metamask or its troubles with your account. Please contact support.
                      <a href="https://t.me/glorymlory">telegram chat</a>
                    </ModalBody>
                    <ModalFooter>
                      <Button color="secondary" onClick={this.toggleModal}>Ok</Button>
                    </ModalFooter>
                  </Modal>
                </div>
              </Form>
              {this.state.errorMessage && (
                <Alert className="alert-sm clearfix" color="danger">
                  <span className="fw-semi-bold">Error:</span> {this.state.errorMessage}
                </Alert>
              )}
            </Widget>

          </Col>

        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isFetching: state.pools.isFetching,
    message: state.pools.message,
    login: state.user.login
  };
}

export default connect(mapStateToProps)(withStyles(s)(withMeta(StepOne)));
