import React, {Component} from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import cx from 'classnames';
import PropTypes from "prop-types";
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';
import {
  Row,
  Col,
  Table,
  Progress,
  Badge,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap';

import Widget from '../../../components/Widget/Widget';
import s from './Static.scss';
import {getAllPools} from "../../../actions/pools";
import tgIcon from '../../../images/telegramIcon.png';

class PoolsTable extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    pools: PropTypes.array, // eslint-disable-line
    isFetching: PropTypes.bool,

  };

  static defaultProps = {
    isFetching: true
  };

  static meta = {
    title: 'Pools list',
    description: 'About description',
  };

  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.checkAll = this.checkAll.bind(this);
  }

  componentWillMount() {
    this.setState({
      activeTab : "1"
    })
    this.props.dispatch(getAllPools());
  }

  parsePoolList() {
    return this.props.pools.map((pool) => (
      <tr key={pool.id}>
        <td><Link to={`/app/pools/${pool.id}`}>{pool.poolName}</Link></td>
        {/*/!*{(pool.currencyEth || pool.currencyDollar || pool.currencyGo) &&*!/*/}
          {/*<td>{pool.currencyEth && (pool.currencyEth)},{pool.currencyDollar && (pool.currencyDollar)},*/}
            {/*{pool.currencyGo && (pool.currencyGo)}</td>*/}
        {/*/!*}*!/*/}
        <td>{pool.totalAmount}</td>
        <td>{pool.bonus}%</td>
        <td>{pool.ownerFee}%</td>
        <td>{pool.rating}</td>
        <td>{pool.startDate}</td>
        <td><a href={pool.webRef}><img src={tgIcon}/></a></td>
      </tr>
    ))
  }

  // parsePoolsColumnsName() {
  //   const columns = {
  //     columns: [{
  //       dataField: 'id',
  //       text: 'Owner name',
  //       sort: true
  //     }, {
  //       dataField: 'poolName',
  //       text: 'Pool name'
  //     }, {
  //       dataField: 'price',
  //       text: 'min Contr.'
  //     }, {
  //       dataField: 'bonus',
  //       text: 'Bonus'
  //     }, {
  //       dataField: 'fee',
  //       text: 'fee'
  //     }, {
  //       dataField: 'raised',
  //       text: 'Raised'
  //     }, {
  //       dataField: 'rating',
  //       text: 'Rating'
  //     }, {
  //       dataField: 'date',
  //       text: 'Date'
  //     }, {
  //       dataField: 'address',
  //       text: 'Adr',
  //     }]
  //   };
  //   return columns.columns.map(column => (
  //     <th key={Math.random()}>
  //       {column.text}
  //     </th>
  //   ))
  // }

  parseDate(date) {
    this.dateSet = date.toDateString().split(' ');
    return `${date.toLocaleString('en-us', {month: 'long'})} ${this.dateSet[2]}, ${this.dateSet[3]}`;
  }

  checkAll(ev, checkbox) {
    const checkboxArr = (new Array(this.state[checkbox].length)).fill(ev.target.checked);
    this.setState({
      [checkbox]: checkboxArr,
    });
  }

  changeCheck(ev, checkbox, id) {
    this.state[checkbox][id] = ev.target.checked;
    if (!ev.target.checked) {
      this.state[checkbox][0] = false;
    }
    this.setState({
      [checkbox]: this.state[checkbox],
    });
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  render() {
    return (
      <div>
        <Row>
          <Col sm={12}>
            <Widget
              title={<h5>Pool <span className="fw-semi-bold">List</span></h5>} settings close
            >
              <Nav tabs>
                <NavItem>
                  <NavLink
                    className={cx({active: this.state.activeTab === '1'})}
                    onClick={() => {
                      this.toggle('1');
                    }}
                  >
                    All pools
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={cx({active: this.state.activeTab === '2'})}
                    onClick={() => {
                      this.toggle('2');
                    }}
                  >
                    Open pools
                  </NavLink>
                </NavItem>
              </Nav>
              <div className="table-responsive">
                <TabContent activeTab={this.state.activeTab}>
                  <TabPane tabId="1">
                    <Table className="table-hover">
                      <thead>
                      <tr>
                        <th>Pool name</th>
                        <th>Hardcap</th>
                        <th>Bonus</th>
                        <th>Fee</th>
                        <th>Rating</th>
                        <th>Start date</th>
                        <th>More info</th>
                      </tr>
                      </thead>
                      < tbody>
                      {this.props.pools &&
                      this.parsePoolList()
                      }
                      {this.props.pools &&
                      !this.props.pools.length && (
                      <tr>
                        <td colSpan="100">No pools yet</td>
                      </tr>
                      )}
                      {this.props.isFetching && (
                        <tr>
                          <td colSpan="100">Loading...</td>
                        </tr>
                      )}
                      </tbody>
                    </Table>
                  </TabPane>
                  <TabPane tabId="2">
                    <Table className="table-hover">
                      <thead>
                      <tr>
                        <th>#</th>
                        <th>f Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Status</th>

                      </tr>
                      </thead>
                      {/* eslint-disable */}
                      <tbody>
                      <tr>
                        <td>1</td>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td><a href="#">ottoto@example.com</a></td>
                        <td><Badge color="gray" className="text-gray" pill>Pending</Badge></td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td><a href="#">fat.thor@example.com</a></td>
                        <td><Badge color="gray" className="text-gray-light" pill>Unconfirmed</Badge></td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td><a href="#">larry@example.com</a></td>
                        <td><Badge color="gray" className="text-gray" pill>New</Badge></td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>Peter</td>
                        <td>Horadnia</td>
                        <td><a href="#">peter@example.com</a></td>
                        <td><Badge color="gray" className="text-gray-light" pill>Active</Badge></td>
                      </tr>
                      </tbody>
                      {/* eslint-enable */}
                    </Table>
                  </TabPane>
                </TabContent>
              </div>
            </Widget>
          </Col>
        </Row>
      </div>
    );
  }

}

function mapStateToProps(state) {
  return {
    isFetching : state.pools.isFetching,
    pools: state.pools.pools,
  };
}

export default connect(mapStateToProps)(withStyles(s)(PoolsTable));
