const path = require('path');
const solc = require('solc');
const fs = require('fs-extra');

const buildPath = path.resolve(__dirname, 'build');
fs.removeSync(buildPath);

// const campaignPath = path.resolve(__dirname, 'contracts', 'PoolFactory.sol');
const input = {
  language: 'Solidity',
  sources: {
    'Ownable.sol': { content: fs.readFileSync(__dirname + '/contracts/Ownable.sol', 'utf8')},
    'Fund.sol': { content: fs.readFileSync(__dirname + '/contracts/Fund.sol', 'utf8')},
    'PoolFactory.sol': { content: fs.readFileSync(__dirname + '/contracts/PoolFactory.sol', 'utf8')},
    'SafeMath.sol': { content: fs.readFileSync(__dirname + '/contracts/SafeMath.sol', 'utf8')}
  },
  settings: {
    outputSelection: {
      // "*": {
      //   "*": [ "metadata", "evm.bytecode" ]
      // },
      // Enable the abi and opcodes output of MyContract defined in file def.
      '*': {
        '*': [ 'abi', 'evm.bytecode' ]
      }
      // Enable the source map output of every single contract.
      // "*": {
      //   "*": [ "evm.bytecode.sourceMap" ]
      // },
      // Enable the legacy AST output of every single file.
      // "Fund": {
      //   "": [ "legacyAST" ]
      // }
    }
  }
};
const output = JSON.parse(solc.compile(JSON.stringify(input))).contracts;
// console.log(output)
fs.ensureDirSync(buildPath);

for (let contract in output) {
  for(let contractName in output[contract]) {
    fs.outputJsonSync(
      path.resolve(buildPath, `${contractName}.json`),
      output[contract][contractName],
      {
        spaces: 2
      }
    )
  }
}
