import web3 from './web3';
import PoolFactory from './build/PoolFactory.json';

const instance = new web3.eth.Contract(
  PoolFactory.abi,
  '0x930e09132446555e4f9fc5593e94b1fd0aacdbaf'
);

export default instance;
