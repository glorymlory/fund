pragma solidity >=0.4.21 <0.6.0;

import "./Fund.sol";

/** @title Fund facoryt contract */
contract PoolFactory is Ownable{

    //fund contract that can deploy
    Fund public fund;

    // The fund  will transfer collected eth to this address
    address public targetAddress;

    // The address of the wallet, where poolPercent will be sent
    address public platformAddress;

    // The maximum poolBalance the pool can have. Contributions beyond this amount won't be accepted
    uint public poolHardCap;

    // percent that platformAddress will receive from poolBalance
    uint public smartPoolPercent;

    address[] public deployedPools;
    address public lastDeployedFund;

    constructor() public {
        platformAddress = 0x45A3406B06E5Fe9718723Ae884a10FB0F6020eb8;
    }

     /** @dev Deploy new Fund to blockchain.
        */
    function deployPool(address _targetAddress,
                       uint _poolHardCap,
                        uint _smartPoolPercent
                     ) public returns(address){
        fund = new Fund(platformAddress,
                    _targetAddress,
                    _poolHardCap,
                    _smartPoolPercent
                     );
        deployedPools.push(address(fund));
        setOwner(msg.sender);
        lastDeployedFund = address(fund);
        return address(fund);
    }
    
    function setPlatformAddress(address _platformAddress) onlyAdmin public  {
        platformAddress = _platformAddress;
    }

     /** @dev Transfer ownership to address that deployPool function called.
              * @param newOwner New owner.
              */
    function setOwner(address newOwner) internal{
        fund.transferOwnership(newOwner);
    }
    
    function getDeployedPools() public view returns (address[] memory) {
        return deployedPools;
    }
}