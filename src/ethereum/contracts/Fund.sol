pragma solidity >=0.4.21 <0.6.0;

import "./SafeMath.sol";
import "./Ownable.sol";

/** @title Fund contract */
contract Fund is Ownable  {
    using SafeMath for uint256;

    // The fund  will transfer collected eth to this address
    address public targetAddress;

    // The address of the wallet, where poolPercent will be sent
    address public platformAddress;

    // The balance in wei the pool currently manages
    uint public poolBalance;

    // The balance in wei each pool contributor has transferred
    mapping (address => uint) public contributorsBalance;

    uint public amountOfContributors = 0;

    // The maximum poolBalance the pool can have. Contributions beyond this amount won't be accepted
    uint public poolHardCap;

    // amount eth in wei that will be sent to platformAddress
    uint256 public smartPoolFees;

    // percent that platformAddress will receive from poolBalance
    uint public smartPoolPercent;

    enum Status {Editable, OnGoing, Locked, Closed}

    Status public poolStatus;


    modifier poolEditable() {
        require(poolStatus == Status.Editable);
        _;
    }

    modifier poolOnGoing() {
        require(poolStatus == Status.OnGoing);
        _;
    }

    modifier poolLocked() {
        require(poolStatus == Status.Locked);
        _;
    }

    modifier poolClosed() {
        require(poolStatus == Status.Closed);
        _;
    }

    function setTargetAddress(address _targetAddress) public onlyAdmin poolEditable {
        targetAddress = _targetAddress;
    }
    
    function setPoolHardCap(uint _poolHardCap) public onlyAdmin poolEditable {
        poolHardCap = _poolHardCap;
    }
    
    function setSmartPoolFees(uint _smartPoolFees) public onlyAdmin poolEditable {
        smartPoolFees = _smartPoolFees;
    }

    function setSmartPoolPercent(uint _smartPoolPercent) public onlyAdmin poolEditable {
        smartPoolPercent = _smartPoolPercent;
    }

    /** @dev Gettin general fund info.
      * @return poolBalance
      * @return owner
      * @return poolStatus
      * @return poolHardCap
      */
    function getPoolInfo()
    public
    view
    returns (uint256, address, Status, uint)
    {
        return (poolBalance, owner, poolStatus, poolHardCap);
    }


    constructor (
        address _platformAddress,
        address _targetAddress,
        uint _poolHardCap,
        uint _smartPoolPercent
        ) public {

        uint256 floatDigits = 100;

        require(_smartPoolPercent < 100 * floatDigits);
        require(_smartPoolPercent > 0);

        targetAddress = _targetAddress;
        platformAddress = _platformAddress;
        poolHardCap = _poolHardCap;
        smartPoolPercent = _smartPoolPercent;
        poolStatus = Status.Editable;
    }


    /* Pool status switchers */
    function setPoolOnGoing() public onlyAdmin poolEditable {
        poolStatus = Status.OnGoing;
    }

    function lockPool() public onlyAdmin poolOnGoing {
        poolStatus = Status.Locked;
    }
    

    function unlockPool() public onlyAdmin poolLocked {
        poolStatus = Status.OnGoing;
    }


    function() external payable {
        contributeInPool(msg.sender);
    }

    /** @dev Contributing eth to fund.
      * @param contributor Address of contributor wallet.
      *  NOTE: fund status have to be OnGoing.
      */
    function contributeInPool(address contributor) public payable poolOnGoing {
        // Pool can't exceed hard cap
        require(poolBalance.add(msg.value) <= poolHardCap);

        //Register how much eth the pool has
        poolBalance = poolBalance.add(msg.value);

        //If it is the first time this account contributes, increase num. of contributors
        if (contributorsBalance[contributor] == 0) {
            amountOfContributors++;
        }

        //Register how much eth has each contributor put into the pool
        contributorsBalance[contributor] = contributorsBalance[contributor].add(msg.value);
    }

     /** @dev Sending ethereum to targetAddres.
         *  NOTE: fund status have to be Locked.
          */
    function sendPoolBalanceToTarget() public onlyAdmin poolLocked {
        require(address(this).balance >= poolBalance); // its a question
        
        calculateFees();
        poolBalance = poolBalance.sub(smartPoolFees);

        // BE CAREFUL, OPENING RE-ENTRANCY DOOR
        // targetICO.transfer(poolBalance);
       
        poolStatus = Status.Closed;
        
        targetAddress.call.value(poolBalance)("");
        platformAddress.call.value(smartPoolFees)("");
        poolBalance = 0;
    }
    
  
 /** @dev Sending refund to contributor.
    *  NOTE: fund status have to be Locked.
      */
    function withdrawEther() public poolLocked {
        require(contributorsBalance[msg.sender] > 0);

        uint ethToWithdraw = contributorsBalance[msg.sender];
        contributorsBalance[msg.sender] = 0;

        //Remove contribution from poolBalance
        poolBalance = poolBalance.sub(ethToWithdraw);
        amountOfContributors--;

        msg.sender.transfer(ethToWithdraw);
    }

    /**
     * Calculates and set fees owner and fund.
     *
     *  NOTE: fee should be specified with two additional digits.
     *  For instance 
     *   if fee percentage is 3.25% - than parameter value should be 325
     *   if fee percentage is 2% - than parameter value should be 200.
     * We have to use such approach as solidity doesn't support float values. 
     */
    function calculateFees() internal {
        uint256 floatDigits = 100;
        
        require(smartPoolPercent < floatDigits.mul(100));
        require(smartPoolPercent > 0);
        
        smartPoolFees =  ((poolBalance*smartPoolPercent) / (floatDigits.mul(100)));
    }
}