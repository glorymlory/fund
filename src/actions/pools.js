export const poolConstants = {
  POOL_GET_REQUEST: 'POOL_GET_REQUEST',
  POOL_GET_SUCCESS: 'POOL_GET_SUCCESS',
  POOL_GET_FAILURE: 'POOL_GET_FAILURE',
  POOL_GETALL_REQUEST: 'POOL_GETALL_REQUEST',
  POOL_GETALL_SUCCESS: 'POOL_GETALL_SUCCESS',
  POOL_GETALL_FAILURE: 'POOL_GETALL_FAILURE',
  POOL_CREATE_REQUEST: 'POOL_CREATE_REQUEST',
  POOL_CREATE_SUCCESS: 'POOL_CREATE_SUCCESS',
  POOL_CREATE_FAILURE: 'POOL_CREATE_FAILURE',
  POOL_CREATE_INITIAL: 'POOL_CREATE_INITIAL',
  POOL_GET_OWNER_REQUEST:"POOL_GET_OWNER_REQUEST",
  POOL_GET_OWNER_SUCCESS:"POOL_GET_OWNER_SUCCESS",
  POOL_GET_OWNER_FAILURE:"POOL_GET_OWNER_FAILURE",

};


export function getPool(poolId) {
  function request() {
    return {type: poolConstants.POOL_GET_REQUEST, isFetching: true}
  }

  function success(pool) {
    return {type: poolConstants.POOL_GET_SUCCESS, isFetching: false, pool}
  }

  function failure(error) {
    return {type: poolConstants.POOL_GET_FAILURE, isFetching: false, error}
  }

  const config = {
    method: 'post',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      query: `{pool(_id:"${poolId}"){ id,
                  poolName,
                  deployedPoolAddress,
                  currencyEth,
                  currencyDollar,
                  currencyGo,
                  ownerWalletAddress,
                  ownerFee,
                  totalAmount,
                  maxPerContr,
                  minPerContr,
                  distributeCheckbox,
                  extraAdminsCheckbox,
                  whitelistCheckbox,
                  lockAdrCheckbox,
                  KYCCheckbox,
                  softcap,
                  privatePool,
                  startDate,
                  endDate,
                  contributorsRestrictions,
                  bonus,
                  rating,
                  webRef
                  }}`,
    }),
    credentials: 'include',
  };

  return dispatch => {
    dispatch(request());

    return fetch('/graphql', config)
      .then(response =>
        response.json().then(responseJson => ({
          pool: responseJson.data.pool,
          responseJson,
        })),
      )
      .then(({pool, responseJson}) => {
        if (!responseJson.data.pool) {
          // If there was a problem, we want to
          // dispatch the error condition
          dispatch(failure(pool.message));
          return Promise.reject(pool);
        }
        // Dispatch the success action
        // console.log("pool:::::",pool.poolName)
        // console.log("poolJSONSTRINGIFY:::::",JSON.stringify(pool))
        // console.log("poolJSONPARSE:::::",JSON.parse(pool))
        dispatch(success(pool));
        return Promise.resolve(pool);
      })
      .catch(err => console.error('Error: ', err));
  };
}

export function getAllPools() {
  function request() {
    return {type: poolConstants.POOL_GETALL_REQUEST, isFetching: true}
  }

  function success(pools) {
    return {type: poolConstants.POOL_GETALL_SUCCESS, isFetching: false, pools}
  }

  function failure(error) {
    return {type: poolConstants.POOL_GETALL_FAILURE, isFetching: false, error}
  }

  const config = {
    method: 'post',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      query: '{pools{id,poolName,deployedPoolAddress,currencyEth,currencyDollar,currencyGo,totalAmount,bonus,ownerFee,rating,startDate,webRef}}',
    }),
    credentials: 'include',
  };

  return dispatch => {
    dispatch(request());

    return fetch('/graphql', config)
      .then(response =>
        response.json().then(responseJson => ({
          pools: responseJson.data.pools,
          responseJson,
        })),
      )
      .then(({pools, responseJson}) => {
        if (!responseJson.data.pools) {
          // If there was a problem, we want to
          // dispatch the error condition
          dispatch(failure(pools.message));
          return Promise.reject(pools);
        }
        // Dispatch the success action
        dispatch(success(pools));
        return Promise.resolve(pools);
      })
      .catch(err => console.error('Error: ', err));
  };
}

export function createPool(poolData) {
  function requestCreatePool(pools) {
    return {type: poolConstants.POOL_CREATE_REQUEST, isFetching: true, pools}
  }

  function successCreatePool(pools) {
    return {type: poolConstants.POOL_CREATE_SUCCESS, isFetching: false, pools}
  }

  function failureCreatePool(error) {
    return {type: poolConstants.POOL_CREATE_FAILURE, isFetching: false, error}
  }

  function initialCreatePool() {
    return {type: poolConstants.POOL_CREATE_INITIAL, isFetching: false}
  }

  const config = {
    method: 'post',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      query: `mutation {
                addPool(
                  id : "${poolData.id}",
                  poolName : "${poolData.poolName}",
                  deployedPoolAddress: "${poolData.deployedPoolAddress}",  
                  currencyEth: ${poolData.currencyEth}, 
                  currencyDollar: ${poolData.currencyDollar}, 
                  currencyGo: ${poolData.currencyGo}, 
                  ownerWalletAddress: "${poolData.ownerWalletAddress}", 
                  ownerFee: "${poolData.ownerFee}",
                  totalAmount: "${poolData.totalAmount}",
                  maxPerContr: "${poolData.maxPerContr}", 
                  minPerContr: "${poolData.minPerContr}",
                  distributeCheckbox: ${poolData.distributeCheckbox},  
                  extraAdminsCheckbox: ${poolData.extraAdminsCheckbox}, 
                  whitelistCheckbox: ${poolData.whitelistCheckbox}, 
                  lockAdrCheckbox: ${poolData.lockAdrCheckbox},  
                  KYCCheckbox: ${poolData.KYCCheckbox}, 
                  softcap: "${poolData.softcap}", 
                  privatePool: ${poolData.privatePool}, 
                  startDate: "${poolData.startDate}", 
                  endDate: "${poolData.endDate}", 
                  contributorsRestrictions: "${poolData.contributorsRestrictions}", 
                  bonus: "${poolData.bonus}", 
                  rating: "${poolData.rating}",
                  webRef: "${poolData.webRef}",
                  UserId: "${poolData.UserId}" 
                ){
                  id,
                  poolName,
                  deployedPoolAddress,
                  currencyEth,
                  currencyDollar,
                  currencyGo,
                  ownerWalletAddress,
                  ownerFee,
                  totalAmount,
                  maxPerContr,
                  minPerContr,
                  distributeCheckbox,
                  extraAdminsCheckbox,
                  whitelistCheckbox,
                  lockAdrCheckbox,
                  KYCCheckbox,
                  softcap,
                  privatePool,
                  startDate,
                  endDate,
                  contributorsRestrictions,
                  bonus,
                  rating,
                  webRef,
                  UserId
                }
              }`,
    }),
    credentials: 'include',
  };

  return dispatch => {
    // We dispatch requestCreatePost to kickoff the call to the API
    dispatch(requestCreatePool(poolData));

    return fetch('/graphql', config)
      .then(response => response.json().then(pool => ({ pool, response })))
      .then(({ pool, response }) => {
        if (!response.ok) {
          // If there was a problem, we want to
          // dispatch the error condition
          dispatch(failureCreatePool(pool.message));
          return Promise.reject(pool);
        }
        // Dispatch the success action
        dispatch(successCreatePool(pool));
        setTimeout(() => {
          dispatch(initialCreatePool());
        }, 5000);
        return Promise.resolve(pool);
      })
      .catch(err => console.error('Error: ', err));
  };
}

export function getOwnerPools(owner) {
  function request() {
    return {type: poolConstants.POOL_GET_OWNER_REQUEST, isFetching: true}
  }

  function success(ownerPools) {
    return {type: poolConstants.POOL_GET_OWNER_SUCCESS, isFetching: false, ownerPools}
  }

  function failure(error) {
    return {type: poolConstants.POOL_GET_OWNER_FAILURE, isFetching: false, error}
  }

  const config = {
    method: 'post',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      query: `{ownerPools(owner:"${owner}"){ id,
                  poolName,
                  deployedPoolAddress,
                  ownerWalletAddress,
                  startDate,
                  endDate 
                  }}`,
    }),
    credentials: 'include',
  };

  return dispatch => {
    dispatch(request());

    return fetch('/graphql', config)
      .then(response =>
        response.json().then(responseJson => ({
          ownerPools: responseJson.data.ownerPools,
          responseJson,
        })),
      )
      .then(({ownerPools, responseJson}) => {
        if (!responseJson.data.ownerPools) {
          // If there was a problem, we want to
          // dispatch the error condition
          console.log("poolsACTION:::",ownerPools)
          dispatch(failure(ownerPools.message));
          return Promise.reject(ownerPools);
        }
        // Dispatch the success action
        // console.log("pool:::::",pool.poolName)
        // console.log("poolJSONSTRINGIFY:::::",JSON.stringify(pool))
        // console.log("poolJSONPARSE:::::",JSON.parse(pool))
        dispatch(success(ownerPools));
        return Promise.resolve(ownerPools);
      })
      .catch(err => console.error('Error: ', err));
  };
}
