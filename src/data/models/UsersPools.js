import DataType from 'sequelize';
import Model from '../sequelize';

const UsersPools = Model.define(
  'UsersPools',
  {
    id: {
      type: DataType.UUID,
      defaultValue: DataType.UUIDV1,
      primaryKey: true,
    },
    ownerName: {
      type: DataType.STRING(255),
      defaultValue: false,
      validate: { notEmpty: false },
    },
    poolAddress: {
      type: DataType.STRING(255),
      defaultValue: false,
      validate: { notEmpty: false },
    },
  }
);

export default UsersPools;
