import DataType from 'sequelize';
import Model from '../sequelize';

const PoolList = Model.define(
  'PoolList',
  {
    id: {
      type: DataType.UUID,
      defaultValue: DataType.UUIDV1,
      primaryKey: true,
    },
    poolName: {
      type: DataType.STRING(255),
      defaultValue: false,
      validate: { notEmpty: false },
    },
    deployedPoolAddress: {
      type: DataType.STRING(255),
      defaultValue: false,
      validate: { notEmpty: false },
    },
    currencyEth: {
      type: DataType.BOOLEAN,
      defaultValue: false,
      validate: { notEmpty: false },
    },
    currencyDollar: {
      type: DataType.BOOLEAN,
      defaultValue: false,
      validate: { notEmpty: false },
    },
    currencyGo: {
      type: DataType.BOOLEAN,
      defaultValue: false,
      validate: { notEmpty: false },
    },
    ownerWalletAddress: {
      type: DataType.STRING(255),
      defaultValue: false,
      validate: { notEmpty: false },
    },
    totalAmount: {
      type: DataType.STRING(255),
      defaultValue: false,
      validate: { notEmpty: false },
    },
    maxPerContr: {
      type: DataType.STRING(255),
      defaultValue: false,
      validate: { notEmpty: false },
    },
    minPerContr: {
      type: DataType.STRING(255),
      defaultValue: false,
      validate: { notEmpty: false },
    },
    distributeCheckbox: {
      type: DataType.BOOLEAN,
      defaultValue: false,
      validate: { notEmpty: false },
    },
    extraAdminsCheckbox: {
      type: DataType.BOOLEAN,
      defaultValue: false,
      validate: { notEmpty: false },
    },
    whitelistCheckbox: {
      type: DataType.BOOLEAN,
      defaultValue: false,
      validate: { notEmpty: false },
    },
    lockAdrCheckbox: {
      type: DataType.BOOLEAN,
      defaultValue: false,
      validate: { notEmpty: false },
    },
    KYCCheckbox: {
      type: DataType.BOOLEAN,
      defaultValue: false,
      validate: { notEmpty: false },
    },
    softcap: {
      type: DataType.STRING(255),
      defaultValue: false,
      validate: { notEmpty: false },
    },
    privatePool: {
      type: DataType.BOOLEAN,
      defaultValue: false,
      validate: { notEmpty: false },
    },
    startDate: {
      type: DataType.STRING(255),
      defaultValue: false,
      validate: { notEmpty: false },
    },
    endDate: {
      type: DataType.STRING(255),
      defaultValue: false,
      validate: { notEmpty: false },
    },
    contributorsRestrictions: {
      type: DataType.STRING(255),
      defaultValue: false,
      validate: { notEmpty: false },
    },
    bonus: {
      type: DataType.STRING(255),
      defaultValue: false,
      validate: { notEmpty: false },
    },
    ownerFee: {
      type: DataType.STRING(255),
      defaultValue: false,
      validate: { notEmpty: false },
    },
    rating: {
      type: DataType.STRING(255),
      defaultValue: false,
      validate: { notEmpty: false },
    },
    webRef: {
      type: DataType.STRING(255),
      defaultValue: false,
      validate: { notEmpty: false },
    },
    UserId: {
      type: DataType.STRING(255),
      defaultValue: false,
      validate: { notEmpty: true },
    },
  }
);

export default PoolList;
