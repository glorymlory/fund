/**
 * Flatlogic Dashboards (https://flatlogic.com/admin-dashboards)
 *
 * Copyright © 2015-present Flatlogic, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import DataType from 'sequelize';
import Model from '../sequelize';
import PoolList from './PoolList';

const User = Model.define(
  'User',
  {
    id: {
      type: DataType.UUID,
      defaultValue: DataType.UUIDV1,
      primaryKey: true,
    },
    username: {
      type: DataType.STRING(255),
      unique: true,
      validate: { notEmpty: true },
    },
    email: {
      type: DataType.STRING(255),
      validate: { isEmail: true },
    },
    emailConfirmed: {
      type: DataType.BOOLEAN,
      defaultValue: false,
    },
    password : {
      type: DataType.STRING(255),
      validate: { notEmpty: true },
    },
    role : {
      type: DataType.STRING(255),
      validate: { notEmpty: false },
    }
  },
  {
    indexes: [{ fields: ['username'] }],
  },
);

User.hasMany(PoolList, {foreignKey: 'UserId', sourceKey: 'username'})
PoolList.belongsTo(User, {foreignKey: 'UserId', targetKey: 'username'})

export default User;
