import {
  GraphQLObjectType as ObjectType,
  GraphQLID as ID,
  GraphQLString as StringType,
  GraphQLNonNull as NonNull,
  GraphQLBoolean as BoolType,
  GraphQLInt as IntType,
  GraphQLFloat as FloatType,
} from 'graphql';
import DataType from "sequelize";

const PoolListType = new ObjectType({
  name: 'PoolList',
  description: 'A pool',
  fields: {
    id: {
      type: new NonNull(ID),
      description: 'the name of the pool',
    },
    poolName: {
      type: StringType,
      description: 'The content of the post.',
    },
    deployedPoolAddress: {
      type: StringType,
      description: 'The content of the post.',
    },
    currencyEth: {
      type: BoolType,
      description: 'The content of the post.',
    },
    currencyDollar: {
      type: BoolType,
      description: 'The content of the post.',
    },
    currencyGo: {
      type: BoolType,
      description: 'The content of the post.',
    },
    ownerWalletAddress: {
      type: StringType,
      description: 'The content of the post.',
    },
    totalAmount: {
      type: StringType,
      description: 'The content of the post.',
    },
    maxPerContr: {
      type: StringType,
      description: 'The content of the post.',
    },
    minPerContr: {
      type: StringType,
      description: 'The content of the post.',
    },
    distributeCheckbox: {
      type: BoolType,
      description: 'The content of the post.',
    },
    extraAdminsCheckbox: {
      type: BoolType,
      description: 'The content of the post.',
    },
    whitelistCheckbox: {
      type: BoolType,
      description: 'The content of the post.',
    },
    lockAdrCheckbox: {
      type: BoolType,
      description: 'The content of the post.',
    },
    KYCCheckbox: {
      type: BoolType,
      description: 'The content of the post.',
    },
    softcap: {
      type: StringType,
      description: 'The content of the post.',
    },
    privatePool: {
      type: BoolType,
      description: 'The content of the post.',
    },
    startDate: {
      type: StringType,
      description: 'The content of the post.',
    },
    endDate: {
      type: StringType,
      description: 'The content of the post.',
    },
    contributorsRestrictions: {
      type: StringType,
      description: 'The content of the post.',
    },
    bonus: {
      type: StringType,
      description: 'The content of the post.',
    },
    ownerFee: {
      type: StringType,
      description: 'The content of the post.',
    },
    rating: {
      type: StringType,
      description: 'The content of the post.',
    },
    webRef: {
      type: StringType,
      description: 'The content of the post.',
    },
    UserId: {
      name: 'User id',
      type: new NonNull(StringType),
    },
  },
});

export default PoolListType;
