import {
  GraphQLObjectType as ObjectType,
  GraphQLID as ID,
  GraphQLString as StringType,
  GraphQLNonNull as NonNull,
  GraphQLBoolean as BoolType,
  GraphQLInt as IntType,
  GraphQLFloat as FloatType,
} from 'graphql';

const UsersPoolsType = new ObjectType({
  name: 'UsersPools',
  description: 'A pool',
  fields: {
    id: {
      type: new NonNull(ID),
      description: 'id',
    },
    ownerName: {
      type: StringType,
      description: 'the name of the pool owner',
    },
    poolAddress: {
      type: BoolType,
      description: '',
    },
  },
});

export default UsersPoolsType;
