import {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLInt,
  GraphQLBoolean,
  GraphQLList
} from 'graphql';

const UserType = new GraphQLObjectType({
  name: 'User',
  allowedRoles: ['admin.all', 'user.read'],
  fields: () => ({
    _id: { type: GraphQLID },
    username: {
      type: GraphQLString,
      description: 'Public string that combined with the password, will identify the user.'
    },
    password: {
      type: GraphQLString,
      description: 'Secret string that combined with the username, will identify the user.'
    },
    email: {
      type: GraphQLString
    },
  })
});

export default UserType;
