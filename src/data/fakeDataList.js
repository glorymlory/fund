export const pools = [{
    "id":"First",
    "poolName" : "Bitnews POOL Club",
    "price" : "0.15 ETH",
    "bonus" : "30%",
    "fee" : "4%",
    "raised" : "56 / 310 ETH",
    "rating" : "5",
    "date" : "14.07.17",
}, {
    "id":"Second",
    "poolName" : "Satoshi Pool",
    "price" : "0.15 ETH",
    "bonus" : "30%",
    "fee" : "4%",
    "raised" : "56 / 310 ETH",
    "rating" : "5",
    "date" : "14.07.17",
}, {
    "id":"3",
    "poolName" : "BICO Oracles",
    "price" : "0.15 ETH",
    "bonus" : "30%",
    "fee" : "4%",
    "raised" : "56 / 310 ETH",
    "rating" : "6",
    "date" : "14.07.17",
}, {
    "id":"4",
    "poolName" : "Crypto Whales Pool ",
    "price" : "0.15 ETH",
    "bonus" : "30%",
    "fee" : "4%",
    "raised" : "56 / 310 ETH",
    "rating" : "5",
    "date" : "14.07.17"
}
]

export default pools
