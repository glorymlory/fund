import {GraphQLList as List, GraphQLNonNull as NonNull, GraphQLString as StringType} from 'graphql';
import {resolver} from 'graphql-sequelize';
import {GraphQLID as ID} from 'graphql';
import PoolListType from "../types/PoolListType";
import User from '../models/User';

const ownerPools = {
  type:  new List(PoolListType),
  args: {
    owner: {
      name: 'owner',
      type: new NonNull(StringType),
    }
  },
  resolve: async (root, {owner}) => {
    if (!owner) {
      return "no username found"
    }
    const user = await User.find({
      where: {
        username: owner
      }
    })
    if(!user){
      return "no pools"
    }
    return user.getPoolLists()
  }
};

export default ownerPools;
