import { GraphQLList as List } from 'graphql';
import { resolver } from 'graphql-sequelize';

import PoolListType from "../types/PoolListType";
import PoolList from '../models/PoolList';

const pools = {
  type: new List(PoolListType),
  resolve: resolver(PoolList),
};

export default pools;
