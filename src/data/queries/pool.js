import {GraphQLList as List, GraphQLNonNull as NonNull, GraphQLString as StringType} from 'graphql';
import {resolver} from 'graphql-sequelize';
import {GraphQLID as ID} from 'graphql';
import PoolListType from "../types/PoolListType";
import PoolList from '../models/PoolList';

const pool = {
  type: PoolListType,
  args: {
    _id: {
      name: '_id',
      type: new NonNull(ID),
    }
  },
  resolve: (root, {_id}) => { return PoolList.findById(Object(_id))}
};

export default pool;
