import {
  GraphQLString as StringType,
  GraphQLNonNull as NonNull,
  GraphQLBoolean as BoolType,
  GraphQLInt as IntType,
  GraphQLFloat as FloatType,
} from 'graphql';

import PoolListType from "../types/PoolListType";
import PoolList from "../models/PoolList";
import DataType from "sequelize";

const addPool = {
  type: PoolListType,
  description: 'Add a pool',
  args: {
    id: {
      name: 'Post title',
      type: new NonNull(StringType),
    },
    poolName: {
      name: 'Post title',
      type: new NonNull(StringType),
    },
    deployedPoolAddress: {
      name: 'Post title',
      type: new NonNull(StringType),
    },
    currencyEth: {
      name: 'Post title',
      type: new NonNull(BoolType),
    },
    currencyDollar: {
      name: 'Post title',
      type: new NonNull(BoolType),
    },
    currencyGo: {
      name: 'Post title',
      type: new NonNull(BoolType),
    },
    ownerWalletAddress: {
      name: 'Post title',
      type: new NonNull(StringType),
    },
    totalAmount: {
      name: 'Post title',
      type: new NonNull(StringType),
    },
    maxPerContr: {
      name: 'Post title',
      type: new NonNull(StringType),
    },
    minPerContr: {
      name: 'Post title',
      type: new NonNull(StringType),
    },
    distributeCheckbox: {
      name: 'Post title',
      type: new NonNull(BoolType),
    },
    extraAdminsCheckbox: {
      name: 'Post title',
      type: new NonNull(BoolType),
    },
    whitelistCheckbox: {
      name: 'Post title',
      type: new NonNull(BoolType),
    },
    lockAdrCheckbox: {
      name: 'Post title',
      type: new NonNull(BoolType),
    },
    KYCCheckbox: {
      name: 'Post title',
      type: new NonNull(BoolType),
    },
    softcap: {
      name: 'Post title',
      type: new NonNull(StringType),
    },
    privatePool: {
      name: 'Post title',
      type: new NonNull(BoolType),
    },
    startDate: {
      name: 'Post title',
      type: new NonNull(StringType),
    },
    endDate: {
      name: 'Post title',
      type: new NonNull(StringType),
    },
    contributorsRestrictions: {
      name: 'Post title',
      type: new NonNull(StringType),
    },
    bonus: {
      name: 'Post title',
      type: new NonNull(StringType),
    },
    ownerFee: {
      name: 'Post title',
      type: new NonNull(StringType),
    },
    rating: {
      name: 'Post title',
      type: new NonNull(StringType),
    },
    webRef: {
      name: 'Post title',
      type: new NonNull(StringType),
    },
    UserId: {
      name: 'User id',
      type: new NonNull(StringType),
    },
  },
  resolve: (root, {
    id,
    poolName,
    deployedPoolAddress,
    currencyEth,
    currencyDollar,
    currencyGo,
    ownerWalletAddress,
    ownerFee,
    totalAmount,
    maxPerContr,
    minPerContr,
    distributeCheckbox,
    extraAdminsCheckbox,
    whitelistCheckbox,
    lockAdrCheckbox,
    KYCCheckbox,
    softcap,
    privatePool,
    startDate,
    endDate,
    contributorsRestrictions,
    bonus,
    rating,
    webRef,
    UserId
  }) => PoolList.create({
    id,
    poolName,
    deployedPoolAddress,
    currencyEth,
    currencyDollar,
    currencyGo,
    ownerWalletAddress,
    ownerFee,
    totalAmount,
    maxPerContr,
    minPerContr,
    distributeCheckbox,
    extraAdminsCheckbox,
    whitelistCheckbox,
    lockAdrCheckbox,
    KYCCheckbox,
    softcap,
    privatePool,
    startDate,
    endDate,
    contributorsRestrictions,
    bonus,
    rating,
    webRef,
    UserId}),
};

export default addPool;
