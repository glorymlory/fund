/**
 * Flatlogic Dashboards (https://flatlogic.com/admin-dashboards)
 *
 * Copyright © 2015-present Flatlogic, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import cx from 'classnames';
import React from 'react';
import {connect} from "react-redux";
import PropTypes from "prop-types";
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
  Navbar,
  Nav,
  NavItem,
  NavbarBrand,
} from 'reactstrap';
import {NavLink} from 'react-router-dom';

import s from './HeaderLanding.scss';

class HeaderLanding extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool,
  };

  static defaultProps = {
    isAuthenticated: false,
  };

  render() {
    return (
      <Navbar className={s.root}>
        <NavbarBrand to="/">reactstrap</NavbarBrand>
        <Nav className="ml-auto">

          <NavItem className={cx('', s.headerIcon)}>
            <NavLink to="/pools"><span>Pools</span></NavLink>
          </NavItem>
          <NavItem className={cx('', s.headerIcon)}>
            <NavLink to="https://github.com/reactstrap/reactstrap"><span>GitHub</span></NavLink>
          </NavItem>
          <NavItem className={cx('', s.headerIcon)}>
            {!this.props.isAuthenticated &&
            <NavLink to="/login"><span>Login</span></NavLink>
            }
            {this.props.isAuthenticated &&
            <NavLink to="/app"><span>Dashboard</span></NavLink>
            }
          </NavItem>
        </Nav>
      </Navbar>
    );
  }
}

function mapStateToProps(state) {
  return {
    isAuthenticated: state.user.isAuthenticated,
  };
}

export default connect(mapStateToProps)(withStyles(s)(HeaderLanding));
