import { poolConstants } from '../actions/pools';

export default function pools(
  state = {
    isFetching: false,
  },
  action,
) {
  switch (action.type) {
    case poolConstants.POOL_GET_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message:
          'Something wrong happened or this pool does not exist. Please come back later',
      });
    case poolConstants.POOL_GET_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        message: null,
      });
    case poolConstants.POOL_GET_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        pool : action.pool,
        message: null,
      });
    case poolConstants.POOL_GET_OWNER_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message:
          'Something wrong happened or this pool does not exist. Please come back later',
      });
    case poolConstants.POOL_GET_OWNER_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        message: null,
      });
    case poolConstants.POOL_GET_OWNER_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        ownerPools : action.ownerPools,
        message: null,
      });
    case poolConstants.POOL_GETALL_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        message: null,
      });
    case poolConstants.POOL_GETALL_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        pools : action.pools,
        message: null,
      });
    case poolConstants.POOL_GETALL_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message:
          'Something wrong happened. Please come back later',
      });
    case poolConstants.POOL_CREATE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message:
          'Due to security reasons pools creation is closed in demo version or smth else went wrong. Please, contact us.',
      });
    case poolConstants.POOL_CREATE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        message: null,
      });
    case poolConstants.POOL_CREATE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        message:
          'Pool created successfully',
      });
    case poolConstants.POOL_CREATE_INITIAL:
      return Object.assign({}, state, {
        isFetching: false,
        message: null
      });
    default:
      return state
  }
}
