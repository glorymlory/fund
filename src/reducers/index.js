import { combineReducers } from 'redux';
import user from './user';
import runtime from './runtime';
import navigation from './navigation';
import posts from './posts';
import pools from './pools';

export default combineReducers({
  user,
  runtime,
  navigation,
  posts,
  pools,
});
