## Project Overview
This is a simple MVP of how smart contracts could be somply managed by any user from front-end. 
Project has following functionality: 
- creating and deploying smart contracts with MetaMask wallet,
- investing into selected project with MetaMask confirmation,
- checking different projects and statuses of any of the deplyod contract.

## Features
* React
* React Router
* Bootstrap3
* GraphQL
* Webpack build
* Stylish, clean, responsive layout
* Authentication
* CRUD operations examples

## Quick Start

#### 1. Get the latest version

You can start by cloning the latest version of React Dashboard on your
local machine by running:

#### 2. Run `yarn install`

This will install both run-time project dependencies and developer tools listed
in [package.json](../package.json) file.

#### 3. Run `yarn start`
